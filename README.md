# Who Am I

This is a resume website built using **Vite**, **React**, and **TypeScript**, with the package manager **pnpm**. It showcases my skills, projects, and achievements as a developer.

## How to Use

You can visit the website [here](https://loganmancuso.gitlab.io/whoami) or clone this repository and run it locally.

### Running Locally

To run the website locally, you will need to have **Node.js** and **pnpm** installed on your machine.

1. Clone this repository using `git clone git@gitlab.com:loganmancuso/whoami.git`
2. Navigate to the project directory using `cd whoami`
3. Install the dependencies using `pnpm install`
4. Start the development server using `pnpm run dev`
5. Open your browser and go to `http://localhost:5173`

### Running GitLab CI/CD Process Locally

To run the GitLab CI/CD process locally, you will need to have **Docker** and **GitLab Runner** installed on your machine.

1. Run the build stage using `gitlab-runner exec docker build`
2. Run the deploy stage using `gitlab-runner exec docker deploy`

## Resources

- [Vite](https://vitejs.dev/)
- [React](https://reactjs.org/)
- [TypeScript](https://www.typescriptlang.org/)
- [pnpm](https://pnpm.io/)
- [GitLab CI/CD](https://docs.gitlab.com/ee/ci/)

## Contact

If you have any questions or feedback, feel free to connect with me on [LinkedIn](https://www.linkedin.com/in/mancuso-logan/).